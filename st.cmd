#- Loading of all E3 modules and versions
require busy
require autosave
require asyn
require adcore,3.10.0+1
require admisc
require iocstats
require recsync
require adsis8300
require essioc

iocshLoad("$(essioc_DIR)/common_config.iocsh")

errlogInit(20000)
callbackSetQueueSize(15000)

###############################################################################
# - IOC Instance parameters (should come from CCDB + IOC Factory)
###############################################################################
epicsEnvSet("CONTROL_GROUP", "$(SIS8300_CG=PBI-WS01)")
epicsEnvSet("AMC_NAME", "$(SIS8300_AMC_NAME=Ctrl-AMC-110)")
epicsEnvSet("AMC_DEVICE", "$(SIS8300_AMC_DEVICE=/dev/sis8300-4)")
epicsEnvSet("EVR_NAME", "$(SIS8300_EVR_NAME=Ctrl-EVR-101):")
###############################################################################

#- 10 MB max CA request
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES",     "10000000")
epicsEnvSet("PREFIX",                       "$(CONTROL_GROUP):$(AMC_NAME):")
epicsEnvSet("PORT",                         "$(AMC_NAME)")
epicsEnvSet("MAX_SAMPLES",                  "500000")
#- AD plugin macros
epicsEnvSet("XSIZE",                        "$(MAX_SAMPLES)")
epicsEnvSet("YSIZE",                        "1")
epicsEnvSet("QSIZE",                        "20")
epicsEnvSet("NCHANS",                       "100")
epicsEnvSet("CBUFFS",                       "500")
epicsEnvSet("MAX_THREADS",                  "4")

#- sis8300Configure(const char *portName, const char *devicePath,
#-                  int numSamples, int maxBuffers, size_t maxMemory, int priority, int stackSize)
sis8300Configure("$(PORT)", "$(AMC_DEVICE)", $(MAX_SAMPLES), 0, 0)

#- sis8300 controls
dbLoadRecords("$(adsis8300_DB)/sis8300.template","P=$(PREFIX),R=,PORT=$(PORT),MAX_SAMPLES=$(MAX_SAMPLES)")
dbLoadRecords("$(adsis8300_DB)/sis8300-evr.template","P=$(PREFIX),R=,PORT=$(PORT),EVR_DEV=$(CONTROL_GROUP):$(EVR_NAME)")

#- 10x data channels

#
#
#
#
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=0, NAME=CH1")
#
#
#
#
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=1, NAME=CH2")
#
#
#
#
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=2, NAME=CH3")
#
#
#
#
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=3, NAME=CH4")
#
#
#
#
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=4, NAME=CH5")
#
#
#
#
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=5, NAME=CH6")
#
#
#
#
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=6, NAME=CH7")
#
#
#
#
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=7, NAME=CH8")
#
#
#
#
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=8, NAME=CH9")
#
#
#
#
iocshLoad("$(E3_CMD_TOP)/channel.iocsh", "ADDR=9, NAME=CH10")
#
#
#
#

#asynSetTraceIOMask("$(PORT)",0,2)
#asynSetTraceMask("$(PORT)",0,255)

set_pass1_restoreFile("$(E3_CMD_TOP)/default_settings.sav", "P=$(PREFIX),R=")

#- run IOC
iocInit

